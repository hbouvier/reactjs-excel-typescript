import React from 'react';
import { State, Position, PositionToString } from '../App';

type CellEditorProps = {
  state: State
  position: Position
  onCellClick: (e : React.MouseEvent<HTMLElement>) => void
  onCellChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  onCellBlur: (e: React.ChangeEvent<HTMLInputElement>) => void
  onCellKeyPress: (e: React.KeyboardEvent<HTMLInputElement>) => void

};

const CellEditor = ({
  position,
  state,
  onCellChange,
  onCellBlur,
  onCellKeyPress
}: CellEditorProps) => <td id={PositionToString(position)} className="">
    <input
      id={PositionToString(position)}
      className=""
      type="text"
      autoFocus
      value={state.cells[position.x][position.y]}
      onChange={onCellChange}
      onBlur={onCellBlur}
      onKeyUp={onCellKeyPress}
    /> 
</td>;

export default CellEditor;